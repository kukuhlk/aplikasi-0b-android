package kurniawan.kukuh.aplikasi0b

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class AdapterDataMember(val dataMember: List<HashMap<String,String>>,
                        val mainActivity: MainActivity) :
    RecyclerView.Adapter<AdapterDataMember.HolderDataMember>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataMember.HolderDataMember {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_member,p0,false)
        return HolderDataMember(v)
    }

    override fun getItemCount(): Int {
        return dataMember.size
    }

    override fun onBindViewHolder(p0: AdapterDataMember.HolderDataMember, p1: Int) {
        val data = dataMember.get(p1)
        p0.txIdMember.setText(data.get("id_member"))
        p0.txNama.setText(data.get("nama"))
        p0.txTelp.setText(data.get("telepon"))
        p0.txLevel.setText(data.get("level"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mainActivity.daftarLevel.indexOf(data.get("level"))
            mainActivity.spLevel.setSelection(pos)
            mainActivity.edIdMember.setText(data.get("id_member"))
            mainActivity.edNama.setText(data.get("nama"))
            mainActivity.edTelp.setText(data.get("telepon"))
            Picasso.get().load(data.get("url")).into(mainActivity.imUP)
        })
        //endNew
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataMember(v: View) : RecyclerView.ViewHolder(v){
        val txIdMember = v.findViewById<TextView>(R.id.txIdMember)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txTelp = v.findViewById<TextView>(R.id.txTelp)
        val txLevel = v.findViewById<TextView>(R.id.txLevel)
        val photo = v.findViewById<ImageView>(R.id.imPhoto)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}