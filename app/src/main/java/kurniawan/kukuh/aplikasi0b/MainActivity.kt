package kurniawan.kukuh.aplikasi0b

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var mediaHelper: MediaHelper
    lateinit var memberAdapter : AdapterDataMember
    lateinit var levelAdapter : ArrayAdapter<String>
    var daftarMember = mutableListOf<HashMap<String,String>>()
    var daftarLevel = arrayOf("Silver","Gold","Admin")
    var url = "http://192.168.43.85/0b_web/show_data.php"
    var url2 = "http://192.168.43.85/0b_web/query_upd_del_ins.php"
    var imStr = ""
    var pilihLevel = ""
    var nmFile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        try {
            val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        }catch (e: Exception){
            e.printStackTrace()
        }

        memberAdapter = AdapterDataMember(daftarMember,this) //new
        mediaHelper = MediaHelper(this)
        listMember.layoutManager = LinearLayoutManager(this)
        listMember.adapter = memberAdapter
        levelAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarLevel)
        spLevel.adapter = levelAdapter
        spLevel.onItemSelectedListener = itemSelected

        imUP.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnCari.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataMember("")
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spLevel.setSelection(0)
            pilihLevel = daftarLevel.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihLevel = daftarLevel.get(position)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode==mediaHelper.getRcCamera()){
                imStr = mediaHelper.getBitmapToString(imUP,fileUri)
                nmFile = mediaHelper.getMyFileName()
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataMember("")
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_member",edIdMember.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("telepon",edTelp.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("level",pilihLevel)
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_member",edIdMember.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("telepon",edTelp.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("level",pilihLevel)
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_member",edIdMember.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMember(namaMhs: String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMember.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("id_member",jsonObject.getString("id_member"))
                    mhs.put("nama",jsonObject.getString("nama"))
                    mhs.put("telepon",jsonObject.getString("telepon"))
                    mhs.put("level",jsonObject.getString("level"))
                    mhs.put("url",jsonObject.getString("url"))
                    daftarMember.add(mhs)
                }
                memberAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama",namaMhs)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun requestPermissions() = runWithPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA){
        fileUri = mediaHelper.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT,fileUri)
        startActivityForResult(intent,mediaHelper.getRcCamera())
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUP ->{
                requestPermissions()
            }
            R.id.btnInsert ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDelete("delete")
            }
            R.id.btnCari ->{
                showDataMember(edNama.text.toString().trim())
            }
        }
    }
}
